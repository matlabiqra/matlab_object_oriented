function [ table summa] = snarky_mu(n,m)
persistent error_count;
if isempty(error_count),error_count = 0;end % no error yet
if nargin <1
    error(' Must have at least one input argument');
end
%%
if nargin <2    % nuber of input arguments 
    m = n;
 elseif ~isscalar(m) || m < 1 || m ~= fix(m) % (fix) rounds the elements of m to the nearest integers towards zero.
   error_count = error_count + 1;
   if error_count <3 ,error('M needs to be positive integer')
end
%
if ~isscalar(n) || n<1 || n ~= fix(n) % (fix) rounds the elements of m to the nearest integers towards zero.
   error_count = error_count + 1;
     if error_count <3 ,error('M needs to be positive integer')
     end
if error_count  >=3
    error_count = 0
    error('what part of positve integer')
    table = (1:n)'.*(1:m);

end
end
end