function [table,sumO,sumO1] = polymorphic_var(n,m)
%%
clc;
close all;
if nargin < 1 % no. of input arguments
    error(' Provide atleat any one variables data')
end
%% 
if nargin <2    % nuber of input arguments 
    ...( if any one input argument is missing then first input argument consider as output argumrnt)
    m = n;
end
if nargin > 2
    table = (1:n)'.*(1:m);
    elseif ~isscalar(m) || m<1 || m ~= fix(m) % (fix) rounds the elements of m to the nearest integers towards zero.
    % condition applying in m for not getting error 
    ... 1. m should not be scalar it is then move to case 2
        ... 2. m less then one if it is not scalar or also less thean 1 move to case 3
        ... 3. if it does not follow then it shows an error
    error('M needs to be positive integer')
end
%% for 2 output arguments
if nargout == 2   % number of output arguments of output argument
    table = (1:n)'.*(1:m);
    sumO = sum(table);  % sum of table elements
end
%% for 3 outout arguments
if nargout == 3   % number of output arguments of output argument
    table = (1:n)'.*(1:m);
sumO = sum(table);  % sum of table elements
 sumO1 = sum(table);
end

