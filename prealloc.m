function prealloc
tic
N = 5000;
A = zeros(N,N);  % this A null matrix is used here to reduce execution time
%% if we didnot used  A = zeros(N,N); this command then execution time is lengthy
for ii = 1:N
    for jj = 1:N
        A(ii,jj) = ii*jj;
    end
end
fprintf('display %d\n',A(ii,jj))
toc
%%
tic
N1 = 5000;
%A1 = zeros(N,N);  % this A null matrix is used here to reduce execution time
%% if we didnot used  A = zeros(N,N); this command then execution time is lengthy
for ii1 = 1:N1
    for jj1 = 1:N1
        A1(ii1,jj1) = ii1*jj1;
    end
end
fprintf('display %d\n',A1(ii1,jj1))
toc
end