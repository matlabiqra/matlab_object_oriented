function [table,sumO] = polymorphic_var_1(n,m)
%% k = POLY is multiplication table % t = polymorphic_var_1
% return an n * n by matrix containing the multiplication for the integer
% contrining the multi plication table in t and the dum of alll the elements
if nargin < 1 % no. of input arguments
    error(' Provide atleat any one variables data')
end
%% 
if nargin <2    % nuber of input arguments 
    ...( if any one input argument is missing then first input argument consider as output argumrnt)
    m = n;
 elseif ~isscalar(m) || m < 1 || m ~= fix(m) % (fix) rounds the elements of m to the nearest integers towards zero.
    error('M needs to be positive integer')
end
%
if ~isscalar(n) || n<1 || n ~= fix(n) % (fix) rounds the elements of m to the nearest integers towards zero.
    error('M needs to be positive integer')
end
    table = (1:n)'.*(1:m);
    
%% for 2 output arguments
if nargout == 2   % number of output arguments of output argument
    sumO = sum(table(:));  % sum of table elements
end
end

