%% for loop
% clc;clear
function total = forloop(k)
% total = 0;
% for n =1:k
%    total = total + n ;
% end
% fprintf('total value = %d\n',total)
%% apply debug process in loop for it we can understand loop concept e]in step size form
%% 
% list = rand(1,5);
% for x = list
%     if x>0.5
%         fprintf(' largest no. %f ,\n',x)
%     else
%         fprintf(' smallestt no. %f ,\n',x)
%     end
% end
%%
u = [1 2 3 4 5];
v = [3 4 5 6 7];
for ii = 1:length(u)
    z(ii) = u(ii) + v(ii);  % in this code line Z store values in array form as i change
     z2 = u(ii) + v(ii) ;   % in this expression z change its value in every iterations
end
fibo(z2);
sqrmat(u);
astric(6);
Revastric(6);
[n, total] = possum(10)
end

%% maling fabonacii series
function f = fibo(n)
if (~isscalar(n) || n<1 || n ~=fix(n))
    error('N must be positive number')
end
f(1) = 1;
f(2) = 1;
for ii = 3:n
    f(ii) = f(ii-2) + f(ii-1) ;
end
end
%% matrix formation through loop
function p = sqrmat(A)
[ row, col] = size(A);
for r = 1:row
    for c = 1:col
        p(r,c) = A(r,c) * A(r,c);
        fprintf('%d %d',p(r,c))
    end
end
end
%% astric for loop
function  astric(n)
for mm = 1:n
    for nn = 1:mm
        fprintf('*')
    end
    fprintf('\n')
end
end
%% Reverse astric for loop
function  Revastric(n)
for mm = n:-1:1
    for nn = mm:-1:1
        fprintf('*')
    end
    fprintf('\n')
end
end
%% while  loop for not knowing the limit but known the final output
function [n, total] = possum(limit)
total = 0;
n = 0;
while total <= limit
    n = n + 1;
    total = total + n;
end
fprintf('sum: %d count %d\n',total,n)
end